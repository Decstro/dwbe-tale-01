/* La idea es hacer que cuando se le de ren registrar el formulario pase a la izquierda */

document.getElementById("btnRegistrarse").addEventListener("click",register);
document.getElementById("btnIniciarSesion").addEventListener("click",login);
window.addEventListener("resize",anchoPagina);

var contenedor_login_register = document.querySelector(".contenedorLoginRegister");
var formulario_login = document.querySelector(".formularioLogin");
var formulario_register = document.querySelector(".formularioRegister");
var caja_trasera_login = document.querySelector(".cajaTraseraLogin");
var caja_trasera_register = document.querySelector(".cajaTraseraRegister");


function anchoPagina(){
    if(window.innerWidth > 850){
         caja_trasera_login.style.display = "block";
         caja_trasera_register.style.display = "block";
    }
    else{
        caja_trasera_login.style.display = "block";
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.display = "none";
        formulario_login.style.display = "block";
        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "0px";
    }
}

anchoPagina();

function login(){

    if(window.innerWidth > 850){

        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "10px";
        formulario_login.style.display = "block";
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.opacity = "0";
        
    }
    else{

        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "0px";
        formulario_login.style.display = "block";
        caja_trasera_register.style.display = "block";
        caja_trasera_login.style.display = "none";
    }
    
}

function register(){

    if(window.innerWidth > 850){
        formulario_register.style.display = "block";
        contenedor_login_register.style.left = "410px";
        formulario_login.style.display = "none";
        caja_trasera_register.style.opacity = "0";
        caja_trasera_login.style.opacity = "1";
    }else{
        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "0px";
        formulario_login.style.display = "none";
        caja_trasera_register.style.display = "none";
        caja_trasera_login.style.display = "block";
        caja_trasera_login.style.opacity = "1";
    }
    
}