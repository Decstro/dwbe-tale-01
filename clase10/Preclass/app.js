//Con esto agregamos el archivo json a app.js
let env = require('./env.variables.json');

//Esto nos trae el ambiente en el que esta corriendo la aplicación
const node_env = process.env.NODE_ENV || 'development';
const node_sor = process.env.NODE_ENV || 'production';

//Con el ambiente ya podemmos obtener las variables que le  corresponden
let variables = env[node_env];
console.log(`El puerto de desarrolllo es: ${variables.PORT}`);
console.log(`La URL es: ${variables.SERVERURL}`);

let variables2 = env[node_sor];
console.log(`El puerto de producción es: ${variables2.PORT}`);
console.log(`La URL es: ${variables2.SERVERURL}`);