let lista = [1, 2, 3, 4, 10, 5, 7, 8]
console.log('esta es la lista', lista)

function maximo(lista) {
  let max_i = 0
  for (let i = 1; i < lista.length; i++) {
    let actual = lista[i]
    if (actual > lista[max_i]) {
      max_i = i
    }
  }
  return lista[max_i]
}
console.log('máximo', maximo(lista))

function minimo(lista) {
  let min_i = 0
  for (let i = 1; i < lista.length; i++) {
    let actual = lista[i]
    if (actual < lista[min_i]) {
      min_i = i
    }
  }
  return lista[min_i]
}
console.log('mínimo', minimo(lista))


function busqueda_binaria(lista, objetivo) {
  let L = 0
  let R = lista.length - 1
  while (L <= R) {
    let M = Math.floor((L + R) / 2)
    if (lista[M] < objetivo) {
      L = M + 1
    }
    else if (lista[M] > objetivo) {
      R = M - 1
    }
    else {
      return M
    }
    // console.log(L, M, R)
  }
  return null
}
lista.sort((a, b) => a - b);
console.log('esta es la lista ordenada', lista)
let i = busqueda_binaria(lista, 5)
console.log('índice del valor encontrado', i)




// Implementar una función para encontrar el segundo mayor
// en la lista de ejemplo la respuesta es el número 8
function segundo_maximo(lista) {
  return 0
}

// Implementar una función para encontrar el segundo menor
// en la lista de ejemplo la respuesta es el número 2
function segundo_menor(lista) {
  return 0
}

// Modificar la función de búsqueda binaria para obtener
// el número menor más cercano al parámetro si este no existe
// en la lista de ejemplo si el valor buscado es 6 retornar 5
function busqueda_binaria_aproximada(lista, objetivo) {
  return 0
}
