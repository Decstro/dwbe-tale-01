const express = require('express');
const router = express();
const _ = require('lodash');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const { obtenerProductos, agregarProducto, editarProducto, eliminarProducto} = require('../models/productos.model');


/**
 * @swagger
 * /productos:
 *  get:
 *      summary: Obtener todos los productos del sistema
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: Lista de productos del sistema organizados por tipo
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Producto'
 */
router.get('/', (req, res) => {
    res.json(_.groupBy(obtenerProductos(), 'tipo'));
});

/** 
 * @swagger
 * /productos/admin:
 *   post:
 *       summary: Crea un nuevo producto en el sistema
 *       tags: [Productos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarProducto'
 *       responses:
 *           200:
 *               description: Producto creado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *  
 */
router.post('/admin', autenticarAdministrador, (req, res) => {
    const {  nombre, precio, tipo } = req.body;
    let id = 0;
    let resultado = false;
    let cont = 0;
    do{
        id = obtenerProductos().length + 1 + cont; 
        let verProducto = obtenerProductos().find(u => u.id === id);
        resultado = verProducto === undefined ? true : false;
        if(resultado === false) cont += 1;
    }while(resultado === false);
    if(tipo === "Bebidas" || tipo === "FastFood" || tipo === "Asados"){
        if(obtenerProductos().find(u => u.id === id) !== undefined) res.status(400).json('Error el id del producto ya se encuentra registrado, no pueden existir productos con igual id');
        else{
            if(obtenerProductos().find(u => u.nombre === nombre) !== undefined) res.status(400).json('Error el nombre del producto ya se encuentra registrado, no pueden existir productos con igual nombre');
            else{
                agregarProducto(  nombre, id, precio, tipo );
                res.json('Producto creado exitosamente');
            }
        }
    }else res.status(400).json('Error: Solo existen estos tres tipos de productos Bebidas, FastFood, Asados'); 
});

/** 
 * @swagger
 * /productos/admin:
 *   put:
 *       summary: Edita el nombre y el precio de un producto 
 *       tags: [Productos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/editarProducto'
 *       responses:
 *           200:
 *               description: Producto agregado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *  
 */
router.put('/admin', autenticarAdministrador, (req,res) => {
    const {nombre, id, precio} = req.body;
    if(obtenerProductos().find(u => u.nombre === nombre) !== undefined) res.status(400).json('Error el nombre del producto ya se encuentra registrado, no pueden existir productos con igual nombre');
    else{
        if(editarProducto(nombre, id, precio)) {
            res.json("Producto editado exitosamente")
        }
        else res.status(404).json('Producto no encontrado, id inexistente'); 
    }
});

/**   
 * @swagger
 * /productos/admin/{idProduct}:
 *   delete:
 *       summary: Elimina el producto segun el id enviado por parametros
 *       tags: [Productos]
 *       parameters:
 *         - name: idProduct
 *           in: path
 *           required: true
 *           description: Id del producto a eliminar
 *           schema:
 *               type: string     
 *       responses:
 *           200:
 *               description: Elimina un producto del sistema
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/Producto'
 *           400:
 *               description: Usuario o Contraseña incorrectos       
 * 
 */
router.delete('/admin/:idProduct', autenticarAdministrador, (req, res) => {
    const idProducto = parseInt(req.params.idProduct);
    const indexProducto = obtenerProductos().findIndex(u => u.id === idProducto);
    if(indexProducto >= 0){ 
        eliminarProducto(indexProducto);
        res.json('Producto Eliminado')
    }
    else res.status(404).json('Producto no encontrado');
});

/**
 * @swagger
 * tags:
 *  name: Productos
 *  description: Sección de productos del sistema
 * components:
 *  schemas:
 *      Producto:
 *          type: object
 *          required:
 *              -nombre 
 *              -id
 *              -precio
 *              -tipo 
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              id:
 *                  type: integer
 *                  description: Id del producto
 *              precio:
 *                  type: integer
 *                  description: Precio del producto
 *              tipo:
 *                  type: string
 *                  description: Categoria del producto
 *          example:
 *              nombre: Carne
 *              id: 4
 *              precio: 4000
 *              tipo: Asados
 *      agregarProducto: 
 *          type: object
 *          required: 
 *              -nombre       
 *              -precio
 *              -tipo    
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              precio: 
 *                  type: integer
 *                  description: Precio del producto
 *              tipo:
 *                  type: string
 *                  description: Categoria del producto 
 *          example:
 *              nombre: Carne
 *              precio: 5000
 *              tipo: Asados
 *      editarProducto: 
 *          type: object
 *          required: 
 *              -nombre
 *              -id       
 *              -precio    
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              id:
 *                  type: integer
 *                  description: Id del producto 
 *              precio: 
 *                  type: integer
 *                  description: Precio del producto
 *          example:
 *              nombre: Carne
 *              id: 4              
 *              precio: 5000
 */

module.exports = router;
