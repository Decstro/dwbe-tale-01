const express = require('express');
const router = express();
const _ = require('lodash');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const {  agregarMedioPago, obtenerMediosPago, editarMediosPago, eliminarMediosPago } = require('../models/mediosDePago.model');

router.get('/obtenerMediosPago', autenticarAdministrador, (req, res) => {
    res.json(_.groupBy(obtenerMediosPago(), 'tipo'));
});

router.post('/crearMedioPago', autenticarAdministrador, (req, res) => {
    const {  nombre, tipo, id } = req.body;
    if(tipo === "PSE" || tipo === "Cuenta de Ahorros" || tipo === "Efectivo" || tipo === "Cheque" || tipo === "Cuenta Corriente"){
        if(obtenerMediosPago().find(u => u.id === id) !== undefined) res.status(400).json(`Error el id: ${id} del producto ya se encuentra registrado`);
        else{
            if(obtenerMediosPago().find(u => u.nombre === nombre) !== undefined) res.status(400).json(`El nombre: ${nombre} ya se encuentra registrado en los Medios de Pago`);
            else{
                agregarMedioPago(  nombre, tipo, id );
                res.json('Medio de Pago creado exitosamente');
            }
        }
    }else res.status(400).json('Error en el tipo de medio de pago, solo existen estos tipos: PSE, Cuenta de Ahorros, Efectivo, Cheque y Cuenta Corriente'); 
});

router.put('/editarMedioPago', autenticarAdministrador, (req,res) => {
    const {nombre, id, tipo} = req.body;
    if(obtenerMediosPago().find(u => u.nombre === nombre) !== undefined) res.status(400).json(`Este nombre ${nombre} ya se encuentra registrado en los medios de Pago`);
    else{
        if(tipo === "PSE" || tipo === "Cuenta de Ahorros" || tipo === "Efectivo" || tipo === "Cheque" || tipo === "Cuenta Corriente"){
            if(editarMediosPago(nombre, id, tipo)) {
                res.json("Medio de Pago editado exitosamente")
            }
            else res.status(404).json('Medio de Pago no encontrado, id inexistente'); 
        }else res.status(400).json('Error en el tipo de medio de pago, solo existen estos tipos: PSE, Cuenta de Ahorros, Efectivo, Cheque y Cuenta Corriente'); 
    }
});

router.delete('/eliminarMedioPago/:idMedioPago', autenticarAdministrador, (req, res) => {
    const idMedioPago = parseInt(req.params.idMedioPago);
    const indexdMedioPago = obtenerMediosPago().findIndex(u => u.id === idMedioPago);
    if(indexdMedioPago >= 0){ 
        eliminarMediosPago(indexdMedioPago);
        res.json('Medio de Pago Eliminado')
    }
    else res.status(404).json(`No se encontro un medio de pago con el id: ${req.params.idMedioPago}`);
});

module.exports = router;