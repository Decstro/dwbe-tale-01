const express = require('express');
const router = express();
router.use(express.json());

const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const { obtenerUsuarios, agregarUsuario} = require('../models/usuarios.model');

/**
* @swagger
* /usuarios:
*   get:
*       summary: Retorna todos los usuarios del sistema
*       tags: [Usuarios]
*       responses:
*           200:
*               description: Muestra la lista de los usuarios del sistema
*               content:
*                   application/json:
*                       schema:
*                           type: array
*                           items:
*                               $ref: '#/components/schemas/usuario'
*/

router.get('/', (req, res) => {
    res.json(obtenerUsuarios());
});

/**                   
* @swagger
* /usuarios:
*   post:
*       summary: Agrega un nuevo usuario al sistema
*       tags: [Usuarios]
*       requestBody:
*           required: true
*           content:
*               application/json:
*                   schema:
*                       $ref: '#/components/schemas/usuario'
*       responses:
*           200:
*               description: Usuario creado exitosamente
*           401:
*               description: El usuario digito un valor a una propiedad que ya se encuentra en la base de datos o que esta erroneo
*/

router.post('/', autenticarAdministrador, (req, res) => {
    const { username, password, email, age, telefono, direccion } = req.body;

    if(!obtenerUsuarios().find(u => u.email === email)){
        agregarUsuario(username, password, email, age, telefono, direccion);
        res.json('Usuario agregado exitosamente');
    }else res.status(400).json('El email ya se encuentra en la base de datos');
});


/**
 * @swagger
 * tags:
 *  name: Usuarios
 *  description: Sección de Usuarios
 * components:
 *  schemas:
 *      usuario:
 *          type: object
 *          required: 
 *              -username
 *              -password
 *              -email
 *              -age
 *              -telefono
 *              -direccion
 *              -idPedidoUser
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              age:
 *                  type: integer
 *                  description: Edad del usuario
 *              telefono:
 *                  type: integer
 *                  description: Telefono del usuario
 *              ifadmin:
 *                  type: boolean
 *                  description: Indica si el usuario es administrador o no
 *              direccion:
 *                  type: string
 *                  description: Dirección del domicilio del usuario
 *              idPedidoUser:
 *                  type: integer
 *                  description: Id que conecta los pedidos con el usuario  
 *          example:
 *              username: nobody
 *              password: anything
 *              email: nobody@gmail.com
 *              age: 33
 *              telefono: 3333333333
 *              direccion: Anyplace
 *             
 */


module.exports = router;