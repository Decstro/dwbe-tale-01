const express = require('express');
const router = express();


router.use(express.json());

const {  obtenerUsuarios, agregarUsuario } = require('../models/usuarios.model');


/**             
 * @swagger
 * /login-register/{username}/{password}:
 *   get:
 *       summary: Logea a los usuarios que tienen una cuenta creada en el sistema
 *       security: [] # No security
 *       tags: [Login-Register]
 *       parameters:
 *         - name: username
 *           in: path
 *           required: true
 *           description: Nombre del usuario
 *           schema:
 *               type: string     
 *         - name: password
 *           in: path
 *           required: true
 *           description: Contraseña del usuario
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Usuario logeado correctamente
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/login'
 *           400:
 *               description: Usuario o Contraseña inexistentes
 *           
 */

router.get('/:username/:password', (req, res) => {
    const {username, password} = req.params; 
    const nombre = obtenerUsuarios().find(u => u.username === username);
    if(nombre){
        const contrasena = obtenerUsuarios().find(u => u.password === password);
        if(contrasena) res.json(`Usuario logeado correctamente`);
        else res.status(401).json(`La contraseña es incorrecta`);
    }else{
        res.status(400).json(` El nombre de usuario ${username} no se encuentra en la base de datos`)
    }
});


/**
 * @swagger
 * /login-register:
 *   post:
 *       summary: Registra un nuevo usuario en el sistema
 *       security: [] # No security
 *       tags: [Login-Register]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/register'
 *       responses:
 *           200:
 *               description: Usuario registrado exitosamente
 *           400:
 *               description: Contraseña o Username inexistentes en la base de datos
 */
router.post('/', (req, res) => {
    const { username, password, email, age, telefono, direccion } = req.body;

    if(!obtenerUsuarios().find(u => u.email === email)){
        agregarUsuario(username, password, email, age, telefono, direccion);
        res.json('Nuevo usuario registrado exitosamente');
    } else res.status(400).json(`El email: ${email} ya se encuentra registrado`);
});

/**
 * @swagger
 * tags:
 *  name: Login-Register
 *  description: Sección de registro e ingreso de Usuarios
 * components:
 *  schemas:
 *      login:
 *          type: object
 *          required:
 *              -username
 *              -password
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *          example:
 *              username: Decstro
 *              password: 12345
 *      register:
 *          type: object
 *          required: 
 *              -username
 *              -password
 *              -email
 *              -age
 *              -telefono
 *              -direccion
 *              -idPedidoUser
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              age:
 *                  type: integer
 *                  description: Edad del usuario
 *              telefono:
 *                  type: integer
 *                  description: Telefono del usuario
 *              ifadmin:
 *                  type: boolean
 *                  description: Indica si el usuario es administrador o no
 *              direccion:
 *                  type: string
 *                  description: Dirección del domicilio del usuario
 *              idPedidoUser:
 *                  type: integer
 *                  description: Id que conecta los pedidos con el usuario  
 *          example:
 *              username: nobody
 *              password: anything
 *              email: nobody@gmail.com
 *              age: 33
 *              telefono: 3333333333
 *              direccion: Anyplace 
 *  
 */

module.exports = router;