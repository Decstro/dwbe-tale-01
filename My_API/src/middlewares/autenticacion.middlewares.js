const { obtenerUsuarios } = require('../models/usuarios.model');
const basicAuth = require('express-basic-auth');

const autenticacion = (usuario, contrasena) => {
    const user = obtenerUsuarios().find(u => u.username === usuario && u.password === contrasena);
    if(user === undefined) return false;

    const userMatch = basicAuth.safeCompare(usuario, user.username);
    const passwordMatch = basicAuth.safeCompare(contrasena, user.password);

    return userMatch && passwordMatch;
}

module.exports = autenticacion;
