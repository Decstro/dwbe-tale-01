require('dotenv').config();
const basicAuth = require('express-basic-auth');
const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const autenticacion = require('./middlewares/autenticacion.middlewares');
const swaggerOptions = require('./utils/swaggerOptions');
const usuarioRoutes = require('./routes/usuario.route');
const pedidoRoutes = require('./routes/pedido.route');
const productoRoutes = require('./routes/producto.route');
const mediosPagosRoutes = require('./routes/mediosPago.route');
const ingresarRoutes = require('./routes/loginRegister.route');

const app = express();
const PORT = process.env.PORT || 3500;

app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs)); 

app.use('/login-register', ingresarRoutes);

app.use(basicAuth({ authorizer: autenticacion }));

app.use('/usuarios', usuarioRoutes); 

app.use('/pedidos', pedidoRoutes);

app.use('/productos', productoRoutes);

app.use('/mediosPago', mediosPagosRoutes);

app.listen(PORT, () => {console.log('Escuchando desde el puerto:  http://localhost:' + PORT)});