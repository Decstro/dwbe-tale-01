const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Sprint Project 1 - Protalento",
            description: "API de Acamica para DWBE",
            version:"1.0.0"
        },
        servers: [
            {
                url: "http://localhost:3500",
                description: "Local Server"
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security: [
            {
                basicAuth: []
            }
        ] 
    },
    apis: ["./src/routes/*.js"] //Aqui estaran ubicadas las rutas


};

module.exports = swaggerOptions;