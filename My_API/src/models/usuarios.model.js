const _ = require('lodash');

const usuarios = [
    {
        username: "Isa-Mary15",
        password: "321",
        email: "isa.15@hotmail.com",
        age: 21,
        telefono: 3148234675,
        direccion: "Torices, Carrera 14 #41-32",
        ifadmin: false,
        idPedidoUser: 1
    },
    {
        username: "Daylo02",
        password: "123",
        email: "day02@gmail.com",
        age: 35,
        telefono: 3129231674,
        direccion: "Prado, Carrera 11 #41-33",
        ifadmin: true,
        idPedidoUser: 2
    },
    {
        username: "Abrahan11",
        password: "123",
        email: "abrhan@gmail.com",
        age: 22,
        telefono: 3138235625,
        direccion: "Manga, Carrera 9 #73-21",
        ifadmin: false,
        idPedidoUser: 4
    },
    {
        username: "Decstro",
        password: "12345",
        email: "decstro@gmail.com",
        age: 21,
        telefono: 3148234672,
        direccion: "Torices, Carrera 14 #41-33",
        ifadmin: true,
        idPedidoUser: 3
    }
];

const obtenerUsuarios = () => {
    return usuarios;
}

function agregarUsuario(name, ide, correo, edad, tel, dir) {
    
    let resultado = false;
    let idUsuario = 0;
    let cont = 0;
    do{
        idUsuario = usuarios.length + 1 + cont; 
        let verUsuario = usuarios.find(u => u.idPedidoUser === idUsuario);
        resultado = verUsuario === undefined ? true : false;
        if(resultado === false) cont += 1;
    }
    while(resultado === false);
    
    
    users = 
        {
            username: name,
            password: ide,
            email: correo,
            age: edad,
            telefono: tel,
            direccion: dir,
            ifadmin: false,
            idPedidoUser: idUsuario
        };
    usuarios.push(users);
}

const verificarAdmin = (index) => {
    return usuarios[index].ifadmin === true ? true : false; 
}


module.exports = { obtenerUsuarios, agregarUsuario, verificarAdmin };