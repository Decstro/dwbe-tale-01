const productos = [
    { 
        nombre: "Hamburguesa Doble",
        id: 1,
        precio: 12000,
        tipo: "FastFood"
    },
    { 
        nombre: "Lomo de Cerdo",
        id: 2,
        precio: 12000,
        tipo: "Asados"
        
    },
    { 
        nombre: "Pechuga",
        id: 3,
        precio: 10000,
        tipo: "Asados"
        
    },
    { 
        nombre: "Carne",
        id: 4,
        precio: 15000,
        tipo: "Asados"
        
    },
    { 
        nombre: "Pescado",
        id: 5,
        precio: 14000,
        tipo: "Asados"
        
    },
    { 
        nombre: "ChoriPerro",
        id: 6,
        precio: 9000,
        tipo: "FastFood"
        
    },
    { 
        nombre: "Limonada",
        id: 7,
        precio: 4000,
        tipo: "Bebidas"
        
    },
    { 
        nombre: "Corona",
        id: 8,
        precio: 5000,
        tipo: "Bebidas"
        
    },
    {
        nombre: "Hamburguesa Sencilla",
        id: 9,
        precio: 9000,
        tipo: "FastFood"
    },
    { 
        nombre: "CocaCola",
        id: 10,
        precio: 3000,
        tipo: "Bebidas"
        
    }
];

const agregarProducto = ( name, identifier, price, type) => {
    let producto = 
        {
            nombre: name,
            id: identifier,
            precio: price,
            tipo: type
        };

    productos.push(producto);
}

const editarProducto = (name, id, price) => {
    let aux = 0;
    productos.forEach( (producto) => {
        if(producto.id === id){
            producto.nombre = name;
            producto.precio = price;
            aux = 1;
        }
    });

    const resultado = aux === 0 ? false : true;
    return resultado;
}

const obtenerProductos = () => {
    return productos;
};

const eliminarProducto = (indexProducto) => {
    productos.splice(indexProducto, 1);
}

module.exports = { agregarProducto, obtenerProductos, editarProducto, eliminarProducto };
