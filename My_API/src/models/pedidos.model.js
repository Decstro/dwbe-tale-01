const pedidos = [
    {
        idPedidosUsuario: 3,
        idPedido: 0,
        estado: "Pendiente",
        direccion: "Torices, Carrera 14 #41-33",
        medioPago: "Cheque Bancario",
        carrito: [
            {
                nombre: "Lomo de Cerdo",
                cantidad: 2
            },
            { 
                nombre: "Pescado",
                cantidad: 5
            } 
        ]
    },
    {
        idPedidosUsuario: 3,
        idPedido: 1,
        estado: "Confirmado",
        direccion: "Torices, Carrera 14 #41-33",
        medioPago: "Tarjeta Debito Visa",
        carrito: [
            {
                nombre: "Hamburguesa Doble",
                cantidad: 1
            },
            { 
                nombre: "Pechuga",
                cantidad: 3
            } 
        ]
    },
    {
        idPedidosUsuario: 3,
        idPedido: 2,
        estado: "En Preparación",
        direccion: "Torices, Carrera 14 #41-33",
        medioPago: "Billetes",
        carrito: [
            {
                nombre: "Hamburguesa Doble",
                cantidad: 1
            },
            { 
                nombre: "Pescado",
                cantidad: 5
            },
            { 
                nombre: "CocaCola",
                cantidad: 11
            }  
        ]
    },
    {
        idPedidosUsuario: 3,
        idPedido: 3,
        estado: "Enviado",
        direccion: "Torices, Carrera 14 #41-33",
        medioPago: "Ahorros Davivienda",
        carrito: [
            {
                nombre: "Carne",
                cantidad: 4
            },
            { 
                nombre: "Pescado",
                cantidad: 5
            },
            {
                nombre: "Limonada",
                cantidad: 7
            }
        ]
    },
    {
        idPedidosUsuario: 1,
        idPedido: 4,
        estado: "Entregado",
        direccion: "Torices, Carrera 14 #41-32",
        medioPago: "Ahorros Bancolombia",
        carrito: [
            {
                nombre: "Hamburguesa Doble",
                cantidad: 1
            },
            { 
                nombre: "Pescado",
                cantidad: 5
            } 
        ]
    }
];

const obtenerPedidos = () => {
    return pedidos;
}

const agregarPedido = (idpedidou, idpedido, products, dir, medio) => {
    pedido = 
        {
            idPedidosUsuario: idpedidou,
            idPedido: idpedido,
            estado: "Pendiente",
            direccion: dir,
            medioPago: medio,
            carrito: products
        };

    pedidos.push(pedido);
}

const agregarProducto = (indexPedido, producto) => {

    pedidos[indexPedido].carrito.forEach((product, i) => {
        if(product.nombre === producto.nombre){
            pedidos[indexPedido].carrito[i].cantidad += producto.cantidad;
        }
    });
    pedidos[indexPedido].carrito.push(producto);
}

const obtenerPedidosUsuario = (idPedidoUser) => {
    let resultado = []; 
    for (const pedido of pedidos) {
        if(pedido.idPedidosUsuario === idPedidoUser) resultado.push(pedido);
    }
    if(resultado.length > 0) return resultado;
    else  return "El usuario no tiene pedidos";
} 

const cambiarEstadoPedido = (status, index) => {
    pedidos[index].estado = status;
}

const obtenerProductosPedido = (indexPedido) => {
    return pedidos[indexPedido].carrito;
}

const eliminarProductoPedido = (indexPedido, indexProducto) => {
    pedidos[indexPedido].carrito.splice(indexProducto, 1);
}

const editarCantProducto = (indexPedido, indexProducto, cantidad) => {
    pedidos[indexPedido].carrito[indexProducto].cantidad = cantidad;
}
module.exports = { obtenerPedidos, agregarProducto, editarCantProducto, cambiarEstadoPedido, obtenerPedidosUsuario, agregarPedido, eliminarProductoPedido, obtenerProductosPedido};