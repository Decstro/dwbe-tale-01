const mediosDePago = [
    { 
        tipo: "PSE",
        nombre: "Tarjeta Debito Visa",
        id: 1
    },
    { 
        tipo: "Cuenta de Ahorros",
        nombre: "Ahorros Bancolombia",
        id: 2
    },
    { 
        tipo: "Efectivo",
        nombre: "Billetes",
        id: 3
    },
    { 
        tipo: "Cheque",
        nombre: "Cheque Bancario",
        id: 4
    },
    { 
        tipo: "Cuenta Corriente",
        nombre: "Visa",
        id: 5
    },
    { 
        tipo: "Cuenta de Ahorros",
        nombre: "Ahorros Davivienda",
        id: 6
    },
    { 
        tipo: "PSE",
        nombre: "Tarjeta Credito Visa",
        id: 7
    },
    { 
        tipo: "Cheque",
        nombre: "Cheque en Blanco",
        id: 4
    }
];


const agregarMedioPago = (name, type, identifier) => {
    let medio = 
        {
            tipo: type,
            nombre: name,
            id: identifier
        };
    
    mediosDePago.push(medio);
}

const obtenerMediosPago = () => {
    return mediosDePago;
}

const editarMediosPago = (name, id, tipo) => {
    let aux = 0;
    mediosDePago.forEach( medio => {
        if(medio.id === id){
            medio.nombre = name;
            medio.tipo = tipo;
            aux = 1;
        }
    });

    const resultado = aux === 0 ? false : true;
    return resultado;
}

const eliminarMediosPago = (indexMedio) => {
    mediosDePago.splice(indexMedio, 1);
}

module.exports = { agregarMedioPago, obtenerMediosPago, editarMediosPago, eliminarMediosPago };