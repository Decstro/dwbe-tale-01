class Users{
    constructor(name, lastName, email, country, password, repeatPassword){
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.password = password;
        this.repeatPassword = repeatPassword;
    }
}

let users = [];


document.getElementById("register").addEventListener("click", (e) => {
    e.preventDefault();
    
    if(validateFields()){

        if(validatePassword(document.getElementById("password").value, document.getElementById("repeatPassword").value)){
            
            if(validateEmail(document.getElementById("email").value)){
                alert("Este email ya se encuentra registrado");
            }
            else{

                let user = new Users(
                    document.getElementById("name").value,
                    document.getElementById("lastName").value,
                    document.getElementById("email").value,
                    document.getElementById("country").value,
                    document.getElementById("password").value,
                    document.getElementById("repeatPassword").value
                );
        
                users.push(user);
        
                console.log(users);
                alert("Registro exitoso")
            }
              
        }else{
            alert("Las contraseñas son diferentes");
        }

    }else{
        alert("Faltan algunos campos");
    }

});

document.getElementById("login").addEventListener("click", (e) => {
    e.preventDefault();

    loginPassword = document.getElementById("password2").value;
    loginEmail = document.getElementById("email2").value;
    
    let coincidence = emailConcidence(loginEmail, loginPassword);
    if(coincidence === false) alert("Email o Contraseña inexistentes o incorrectos");
    else alert("Login exitoso, el usuario esta en la posición: " + coincidence);
});

function validateFields(){
    
    if(document.getElementById("name").value === "") return false; 
    if(document.getElementById("lastName").value === "") return false;
    if(document.getElementById("email").value === "") return false;
    if(document.getElementById("country").value === "") return false;
    if(document.getElementById("password").value === "") return false;
    if(document.getElementById("repeatPassword").value === "") return false;
     
    return true;
}

function validatePassword(pass1,pass2){
    decisión = pass1 === pass2 ? true : false;
    return decisión;
}

function validateEmail(valiEmail){
    
    for (let i = 0; i < users.length; i++) {
        let element = users[i];
        if(element.email === valiEmail){
            return true;
        }
    }
    return false;
}

function emailConcidence(loginEmail, loginPassword){
    
    for (let i = 0; i < users.length; i++) {
        let element = users[i];
        if(element.email === loginEmail){
            if(element.password === loginPassword){
                return i;
            }        
        }
    }
    return false;
}
