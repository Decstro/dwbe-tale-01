const _ = require('lodash');

const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length / 2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);
    }
    return mitadTelefonos;
}

const obtenerBajoPrecio = () => {
    let lowPrice = 100000000000 ;
    let telefonoLowPrice = [];
    for (let i = 0; i < telefonos.length; i++) {
        const telefono = telefonos[i];
        if(telefono.precio<lowPrice){
            lowPrice = telefono.precio;
            telefonoLowPrice = telefono;
        }
    }

    return telefonoLowPrice;
}

const obtenerAltoPrecio = () => {
    let highPrice = -1;
    let telefonoHighPrice = [];
    for (let i = 0; i < telefonos.length; i++) {
        const telefono = telefonos[i];
        if(telefono.precio>highPrice){
            highPrice = telefono.precio;
            telefonoHighPrice = telefono;
        }
    }

    return telefonoHighPrice;
}

function agruparSegunGama(){
    let agrupado = [];
    agrupado.push(_.groupBy(telefonos, 'gama'));
    return agrupado;
}

module.exports = { obtenerTelefonos, obtenerMitadTelefonos, obtenerBajoPrecio, obtenerAltoPrecio, agruparSegunGama };
