const express = require('express');
const app = express();
const { obtenerTelefonos, obtenerMitadTelefonos, obtenerBajoPrecio, obtenerAltoPrecio, agruparSegunGama  } = require('./models/telefonos');


app.get('/telefonos', (req, res) => {
    res.json(obtenerTelefonos());
});

app.get('/mitadtelefonos', (req, res) => {
    res.json(obtenerMitadTelefonos());
});

app.get('/telefonosBajoPrecio', (req, res) => {
    res.json(obtenerBajoPrecio());
});

app.get('/telefonosAltoPrecio', (req, res) => {
    res.json(obtenerAltoPrecio());
});

app.get('/telefonosGama', (req, res) => {
    res.json(agruparSegunGama());
});

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});



