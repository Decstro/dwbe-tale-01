const express = require('express');
const app = express();

app.listen(5000, () => console.log("listening on 5000"));


  
app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);
    next();   
}
app.use(logger);

const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = false;
        if(usuarioAdministrador ) {
            console.log(`El usuario está correctamente logueado.`);
        next();
    } else {
        res.send('No está logueado');
        }  
};

app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
      {id: 1, nombre: "Lucas", edad: 35}
    ])
});


app.get('/cursos',(req,res) => {
    res.send([
        {id: 5832, nombre: "Curso Node js", creditos: 3},
        {id: 2341, nombre: "Curso Esenciales Js", creditos: 2},
        {id: 3943, nombre: "Curso Express js", creditos: 4},
        {id: 1043, nombre: "Curso npm", creditos: 2}
    ]);
    console.log(req.path);
});
