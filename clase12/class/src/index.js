const express = require('express');
const app = express();

function imprimirRequest(req,res,next) {
    console.log(`Hora desde middleware: ${Date.now()}`);
    next();
}

function imprimirRequest2(req,res,next) {
    res.json('No estas autorizado');
    console.log(`Ruta : ${req.path}`);
    next();
}

app.use(imprimirRequest);
app.use(imprimirRequest2);

app.get('/ejemplo', (req,res) => {

    console.log('Respuesta ejemplo desde endpoint');
});

app.listen(3000, () => {console.log('Escuchando al puerto 3000')});