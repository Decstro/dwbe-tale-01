const express = require('express');
const app = express();
app.use(express.json());

let autores = [
    {
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        fechaNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                sinopsis: "Se de trata de uno de sus mejores libros...",
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: "El Aleph",
                sinopsis: "Otra recopilación de cuentos...",
                anioPublicacion: 1949   
            }
        ]
    },
    {
        id: 2,
        nombre: "Daniel James",
        apellido: "Brown",
        fechaNacimiento: "22/06/1964",
        libros: [
            {
                id: 1,
                titulo: "Angeles y Demonios",
                sinopsis: "Es una novela de intriga y suspenso...",
                anioPublicacion: 2000
            },
            {
                id: 2,
                titulo: "Codigo Da Vinci",
                sinopsis: "Al combinar los generos de suspenso detectivestico y ...",
                anioPublicacion: 2004   
            }, 
            {
                id: 3,
                titulo: "Inferno",
                sinopsis: "Es la continuación del codigo da vinci...",
                anioPublicacion: 2013
            }
        ]
    },
    {
        id: 3,
        nombre: "Ray",
        apellido: "Bradbury",
        fechaNacimiento: "22/08/1920",
        libros: [
            {
                id: 1,
                titulo: "Fahreinheit 451",
                sinopsis: "Es una novela distopica...",
                anioPublicacion: 1953
            },
            {
                id: 2,
                titulo: "La feria de las tinieblas",
                sinopsis: "Trata de dos muchachos de 13 años...",
                anioPublicacion: 1962   
            }, 
            {
                id: 3,
                titulo: "El arbol de las brujas",
                sinopsis: "Es del genero fantastico...",
                anioPublicacion: 1972
            }
        ]
    }
];

//Endpoint para imprimir todos los autores
app.get('/autores', (req,res) => {
    res.json(autores);
});

//End Point para agregar autores
app.post('/autores', (req,res) => {
    const { id, nombre, apellido, fechaNacimiento, libros} = req.body;
    const authors = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        fechaNacimiento: fechaNacimiento,
        libros: libros
    };
    autores.push(authors); 
    res.json('Usuario agregado exitosamente');
});

//Middleware para verificar que el autor existe
const existenciaUsuario = (req, res, next) => {
    const id = parseInt(req.params.id);
    let buscarAutor = []; buscarAutor = autores.find(u => u.id == id);
    if(buscarAutor) next();
    else res.status(404).json('Usuario no Encontrado desde middleware');
};

//Muestra el autor dado si Id
app.get('/autores/:id', existenciaUsuario, (req, res) => {
    const id = req.params.id;
    const buscarAutor = autores.find(u => u.id == id);
    if(buscarAutor) res.json(buscarAutor);
    else res.status(404).json('Usuario no Encontrado')
});

//Elimina un autor dado su Id
app.delete('/autores/:id', existenciaUsuario,  (req, res) => {
    const id = req.params.id;
    const index = autores.findIndex(u => u.id == id);
    if(index>=0){ 
        autores.splice(index, 1)
        res.json('Usuario eliminado')
    }
    else res.status(404).json('Usuario no Encontrado');
});

//Cambias los datos de un autor dependiendo de su id
app.put('/autores/:id', existenciaUsuario, (req, res) => {
    const { id, nombre, apellido, fechaNacimiento, libros} = req.body;
    const identifer = req.params.id;
    const index = autores.findIndex(u => u.id == identifer);
    if(index>=0){ 
        const authors = {
            id: id,
            nombre: nombre,
            apellido: apellido,
            fechaNacimiento: fechaNacimiento,
            libros: libros
        };
        autores[index] = authors;
        res.json('Usuario actualizado');
    }
    else res.status(404).json('Usuario no Encontrado');
});

//Obtienes  los libros de un autor
app.get('/autores/:id/libros', existenciaUsuario,  (req,res) => {
    const id = req.params.id;
    const buscarAutor = autores.find(u => u.id == id);
    if(buscarAutor) res.json(buscarAutor.libros);
    else res.status(404).json('Usuario no Encontrado');
});

//Agregar libro dependiendo del id del autor
app.put('/autores/:id/libros', existenciaUsuario, (req, res) => {
    const {libros} = req.body;
    const identifer = req.params.id;
    const index = autores.findIndex(u => u.id == identifer);
    if(index>=0){ 
        autores[index].libros.push(libros);
        res.json('Libro agregado');
    }
    else res.status(404).json('Usuario no Encontrado');
});

const vLibroe_Autorc = (req,res,next) => {
    const idAutor = req.params.id;
    const idLibro = req.params.idLibro;
    const indexAutor = autores.findIndex(u => u.id == idAutor);
    if(indexAutor>=0){
        const indexLibro =  autores[indexAutor].libros.findIndex(u => u.id == idLibro);
        if(indexLibro>=0){
            next();
        }
        else res.status(404).json('Libro no Encontrado');
    } 
    else res.status(404).json('Usuario no Encontrado');    
} 


//Obtienes un libro dependiendo de su id y del id del autor
app.get('/autores/:id/libros/:idLibro', existenciaUsuario, vLibroe_Autorc, (req,res) => {
    const idAutor = req.params.id;
    const idLibro = req.params.idLibro;
    const indexAutor = autores.findIndex(u => u.id == idAutor);
    if(indexAutor>=0){
        const indexLibro =  autores[indexAutor].libros.findIndex(u => u.id == idLibro);
        if(indexLibro>=0){
            res.json(autores[indexAutor].libros[indexLibro]);
        }
        else res.status(404).json('Libro no Encontrado');
    } 
    else res.status(404).json('Usuario no Encontrado');
});

//Actualizar libro dependiendo de su id y del id de su autor
app.put('/autores/:id/libros/:idLibro', existenciaUsuario, vLibroe_Autorc,(req,res) => {
    const {libros} = req.body;
    const idAutor = req.params.id;
    const idLibro = req.params.idLibro;
    const indexAutor = autores.findIndex(u => u.id == idAutor);
    if(indexAutor>=0){
        const indexLibro =  autores[indexAutor].libros.findIndex(u => u.id == idLibro);
        if(indexLibro>=0){
            autores[indexAutor].libros[indexLibro] = libros;
            res.json('Libro actualizado');
        }
        else res.status(404).json('Libro no Encontrado');
    } 
    else res.status(404).json('Usuario no Encontrado');
});

//Borras un libro dependiendo de su autor y de su id
app.delete('/autores/:id/libros/:idLibro', existenciaUsuario, vLibroe_Autorc, (req, res) => {
    const idAutor = req.params.id;
    const idLibro = req.params.idLibro;
    const indexAutor = autores.findIndex(u => u.id == idAutor);
    if(indexAutor>=0){
        const indexLibro =  autores[indexAutor].libros.findIndex(u => u.id == idLibro);
        if(indexLibro>=0){
            autores[indexAutor].libros.splice(indexLibro, 1);
            res.json('Libro eliminado');
        }
        else res.status(404).json('Libro no Encontrado');
    }
    else res.status(404).json('Usuario no Encontrado');
});


app.listen(3000,  () => {console.log('Escuchando al puerto 3000')});