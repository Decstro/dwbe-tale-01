class Dogs{
    constructor(nombre, raza, edad, peso, estado){
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.peso = peso;
        this.estado = estado;
    }

    getAdoptedStatus(){
        return  this.estado;
    }

    modifyAdoptedStatus(status){
        while(status !== 1 && status !== 2 && status !== 3){
            status = parseInt(prompt("Recuerde que es 1 para adopción, 2 si esta en proceso, 3 si esta adoptado: "));
        }
        if(status === 1){
            this.estado = "En adopción";
        } 
        else if(status === 2) {
            this.estado = "Proceso de adopción" ;
        }
        else{
            this.estado = "Adoptado";
        }
    }
}


let dogs = [];

do{
    let dog = new Dogs(
                    prompt("Digite el nombre del perro:"),
                    prompt("Digite la raza del perro :"), 
                    prompt("Edad del perro? "),
                    prompt("Cual es el peso del perro?: "));
                    dog.modifyAdoptedStatus( parseInt(prompt("Digite 1 si el perro esta en adopción, Digite 2 si esta en proceso de adopción y Digite 3 si esta adoptado")));
    dogs.push(dog);
}
while(window.confirm("desea agregar otro perro? "));

console.log(dogs);

//IMPRIMIR TODOS LOS PERROS
function printDogs(canines){
    console.log("Perros en la perrera");
    for (let i = 0; i < canines.length; i++) {
        console.log(canines[i]);
    }
}

printDogs(dogs);


//IMPRIMIR LOS PERROS QUE ESTÁN DISPONIBLES PARA ADOPTAR
const inAdoptionDogs = dogs.filter( dog => 
    dog.estado === "En adopción");

console.log("Perros en Adopción");
inAdoptionDogs.forEach( (adoptedDog) => {
    console.log(adoptedDog);
});

//IMPRIMIR LOS PERROS QUE ESTÁN EN PROCESO DE ADOPCIÓN
const dogInProcess = dogs.filter( dog => 
    dog.estado === "Proceso de adopción");

console.log("Perros en Proceso de adopción");
dogInProcess.forEach( (adoptedDog) => {
    console.log(adoptedDog);
});

//IMPRIMIR LOS PERROS QUE ESTÁN ADOPTADOS
const adoptedDogs = dogs.filter( dog => 
    dog.estado === "Adoptado");

console.log("Perros Adoptados");
adoptedDogs.forEach( (adoptedDog) => {
    console.log(adoptedDog);
});
    
