function holaClase(){
    return "Hola Acamica";
}

console.log(holaClase());

//Por defecto esta función retorna un undefined porque no tiene return
const mensaje = (texto) => {
    console.log(`El texto enviado por parametro es: ${texto}`);
}

console.log(mensaje("Hola de nuevo Acamica"));