const mascotas = ['Perros','Gatos','Aves','Peces','Conejos']

mascotas.push('Ratones');

console.log(mascotas);

mascotas.forEach( (mascota, indice) => {
    console.log(`Indice: ${indice} Valor: ${mascota}`);
});