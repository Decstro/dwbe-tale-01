function addition(number1,number2){
    return number1 + number2;
}

function substraction(number1,number2){
    return number1 - number2;
}

function multiplication(number1,number2){
    return number1*number2;
}

function divide(number1,number2){
    return number1/number2;
}

exports.addition = addition;
exports.substraction = substraction;
exports.multiplication = multiplication;
exports.divide = divide;

