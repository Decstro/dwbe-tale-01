const libreria = require('./calculator.js');
const fs = require('fs');

const results = [
    {
        operation: "addition",
        value1: 5,
        value2: 8
    },
    {
        operation: "substraction",
        value1: 7,
        value2: 5
    },
    {
        operation: "multiplication",
        value1: 6,
        value2: 7
    },
    {
        operation: "divide",
        value1: 3,
        value2: 4
    },
];




results.forEach( (result, indice) => {
    
    if(result.operation === "addition") resultado = ` ${indice+1}. ${libreria.addition(result.value1,result.value2)}\n`;
    else if(result.operation === "substraction")  resultado = ` ${indice+1}. ${libreria.substraction(result.value1,result.value2)}\n`;
    else if(result.operation === "multiplication") resultado = ` ${indice+1}. ${libreria.multiplication(result.value1,result.value2)}\n`;
    else if(result.operation === "divide") resultado = ` ${indice+1}. ${libreria.divide(result.value1,result.value2)}\n`;
    console.log(resultado);
    
    fs.appendFileSync('listaOperaciones.txt', resultado, function(err){
        if(err) console.log(err);
        else console.log('Saved!');
    });
});



